# Ejericio 7

from mysound import Sound


class SoundSin(Sound):
    def __init__(self, duration, frequency, amplitude):
        super().__init__(duration)  # Llamamos al constructor de la clase base
        self.sin(frequency, amplitude)  # Generamos la señal sinusoidal

    def soundadd(s1: Sound, s2: Sound) -> Sound:

        # Determina la longitud del sonido resultante
        result_duration = max(s1.duration, s2.duration)

        # Inicializa un nuevo objeto Sound para el sonido resultante
        result_sound = Sound(result_duration)

        # Realiza la suma de las muestras de s1 y s2
        for i in range(result_sound.nsamples):
            if i < s1.nsamples and i < s2.nsamples:
                result_sound.buffer[i] = s1.buffer[i] + s2.buffer[i]
            elif i < s1.nsamples:
                result_sound.buffer[i] = s1.buffer[i]
            elif i < s2.nsamples:
                result_sound.buffer[i] = s2.buffer[i]

        return result_sound
